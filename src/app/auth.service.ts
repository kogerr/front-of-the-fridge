import { Injectable } from '@angular/core';
import { FridgeOpenerService } from './fridge-opener.service';

@Injectable()
export class AuthService {
  redirectUrl: string;

  constructor(private fridgeOpener: FridgeOpenerService) { }

  login(name: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.fridgeOpener.getUser(name).subscribe(validatedUser => {
        localStorage.setItem('name', validatedUser.id);
        resolve(true);
      },
      err => reject(false));
    });
  }

  logout(): void {
    localStorage.removeItem('name');
  }

  public isLoggedIn(): boolean {
    const name = localStorage.getItem('name');
    return name && name.length > 0;
  }

}
