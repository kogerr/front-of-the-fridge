import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FoodItem } from './model/food-item';

const foodItemUrl = '/api/fooditem';
const qrUrl = '/api/fooditem';
const userUrl = '/api/user';

@Injectable({
  providedIn: 'root'
})
export class FridgeOpenerService {

  constructor(private http: HttpClient) { }

  getFoodItems(): Observable<FoodItem[]> {
    return this.http.get<FoodItem[]>(foodItemUrl);
  }

  updateFoodItem(item: FoodItem): Observable<FoodItem> {
    return this.http.patch<FoodItem>(foodItemUrl + '/' + item.id, item);
  }

  deleteFoodItem(item: FoodItem): Observable<FoodItem> {
    return this.http.delete<FoodItem>(foodItemUrl + '/' + item.id);
  }

  sendQr(qr: any): any {
    return this.http.post<any>(foodItemUrl, qr);
  }

  getUser(name: string): Observable<{id: string}> {
    return this.http.post<{id: string}>(userUrl, name);
  }

}
