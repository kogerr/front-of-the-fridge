import { Component, OnInit, ViewChild } from '@angular/core';
import { ZXingScannerComponent } from '../modules/zxing-scanner/zxing-scanner.module';
import { Result } from '@zxing/library';
import { FridgeOpenerService } from '../fridge-opener.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.css']
})
export class ScannerComponent implements OnInit {
  @ViewChild('scanner')
  scanner: ZXingScannerComponent;

  hasDevices: boolean;
  hasPermission: boolean;
  qrResultString: string;
  qrResult: Result;

  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo;

  constructor(private fridgeOpener: FridgeOpenerService, private router: Router) { }

  ngOnInit() {
    this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      this.availableDevices = devices; this.currentDevice = this.availableDevices[this.availableDevices.length - 1];
    });
    this.scanner.hasDevices.subscribe((has: boolean) => this.hasDevices = has);
    this.scanner.scanComplete.subscribe((result: Result) => this.qrResult = result);
    this.scanner.permissionResponse.subscribe((perm: boolean) => this.hasPermission = perm);
  }

  displayCameras(cameras: MediaDeviceInfo[]) {
    this.availableDevices = cameras;
  }

  handleQrCodeResult(resultString: string) {
    this.qrResultString = resultString;
    this.fridgeOpener.sendQr(JSON.parse(resultString)).subscribe(data => {
      this.router.navigate(['/list']);
    }, err => {
      this.qrResultString = JSON.stringify(err);
    });
  }

  onDeviceSelectChange(selectedValue: string) {
    this.currentDevice = this.scanner.getDeviceById(selectedValue);
  }

  stateToEmoji(state: boolean): string {

    const states = {
      // not checked
      undefined: '❔',
      // failed to check
      null: '⭕',
      // success
      true: '✔',
      // can't touch that
      false: '❌'
    };

    return states['' + state];
  }

}
