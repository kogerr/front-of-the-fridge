import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  name: string;
  error: string;

  constructor(private authService: AuthService, private router: Router) { }

  login(): void {
    console.log(this.name);
    if (this.name) {
      this.authService.login(this.name)
        .then((res) => {
          if (res) {
            if (this.authService.redirectUrl) {
              this.router.navigate([this.authService.redirectUrl]);
            }
          } else {
            this.error = 'Failure on authentication. Please try again.';
          }
        });
    } else {
      this.error = 'Please enter name.';
    }
  }

}
