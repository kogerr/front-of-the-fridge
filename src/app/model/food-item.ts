export interface FoodItem {
    id: string;
    name: string;
    imageUrl: string;
    quantity: number;
    expiryDate: Date;
    expired: boolean;
}
