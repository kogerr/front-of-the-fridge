import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FoodListComponent } from './food-list/food-list.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ScannerComponent } from './scanner/scanner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DayComponent } from './day/day.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { AuthenticationInterceptor } from './auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

const appRoutes: Routes = [
  { path: '', component: FoodListComponent, canActivate: [AuthGuard] },
  { path: 'calendar', component: CalendarComponent, canActivate: [AuthGuard]  },
  { path: 'scanner', component: ScannerComponent, canActivate: [AuthGuard]  },
  { path: 'day', component: DayComponent, canActivate: [AuthGuard]  },
  { path: 'list', redirectTo: '/', pathMatch: 'full', canActivate: [AuthGuard]  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
      RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    }, AuthGuard, AuthService
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutingModule { }
