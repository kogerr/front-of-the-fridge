import { Component, OnInit } from '@angular/core';
import { FoodItem } from '../model/food-item';
import { FridgeOpenerService } from '../fridge-opener.service';

@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.css']
})
export class FoodListComponent implements OnInit {
  items: Array<FoodItem>;

  constructor(private fridgeOpener: FridgeOpenerService) { }

  ngOnInit() {
    this.updateAll();
  }

  updateAll(): void {
    this.fridgeOpener.getFoodItems().subscribe(data => {
      if (data) {
        data.forEach(e => e.expiryDate = new Date(e.expiryDate));
        this.items = data.filter(e => e.quantity > 0).sort((a, b) => a.expiryDate.valueOf() - b.expiryDate.valueOf());
      }
    }, err => {
      console.log(err);
    });
  }

  updateItem(item: FoodItem): void {
    this.fridgeOpener.updateFoodItem(item).subscribe(data => this.updateAll());
  }

  deleteItem(item: FoodItem): void {
    this.fridgeOpener.deleteFoodItem(item).subscribe(data => this.updateAll());
  }

}
