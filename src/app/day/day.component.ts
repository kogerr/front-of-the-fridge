import { Component, OnInit } from '@angular/core';
import { FoodItem } from '../model/food-item';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {

  day: number;
  items: Array<FoodItem>;

  ngOnInit(): void {
    this.day = JSON.parse(localStorage.getItem('day'));
    this.items = JSON.parse(localStorage.getItem('items'));
  }
}
