import { Component, OnInit } from '@angular/core';
import { FoodItem } from '../model/food-item';
import { FridgeOpenerService } from '../fridge-opener.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  items: Array<FoodItem>;
  month = new Array<Array<FoodItem>>(31);
  constructor(private fridgeOpener: FridgeOpenerService, private router: Router) { }

  ngOnInit() {
    for (let i = 0; i < 31; i++) {
      this.month[i] = new Array<FoodItem>();
    }
    this.fridgeOpener.getFoodItems().subscribe(data => {
      if (data) {
        data.forEach(e => {
          const day = (new Date(e.expiryDate)).getDate();
          this.month[day - 1].push(e);
        });
      }
      console.log(this.month);
    }, err => {
      console.log(err);
    });
  }

  checkFood(day: number, items: Array<FoodItem>): void {
    if (items.length) {
      localStorage.setItem('day', day.toString());
      localStorage.setItem('items', JSON.stringify(items));
      this.router.navigate(['/day']);
    }
  }

}
