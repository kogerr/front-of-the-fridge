import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const name = localStorage.getItem('name');
        if (name) {
            const cloned = req.clone({headers: req.headers.set('userId', name)});
            return next.handle(cloned);
        } else {
            return next.handle(req);
        }
    }
}
